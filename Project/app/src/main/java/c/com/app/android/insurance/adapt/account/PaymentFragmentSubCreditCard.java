package c.com.app.android.insurance.adapt.account;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import c.com.app.android.insurance.adapt.R;

/**
 * Payment screen's submenu for credit card users.
 */
public class PaymentFragmentSubCreditCard extends Fragment {





    /*

        TODO: Find a way to make user send credit card details in order to send payment to a micro insurance company.

         Conditions needed to achieve:
        - Need to obtain/know the user's credit card number to be forwarded for payment.
        - Need bank provider the user subscribed.

     */





    private EditText etCardNumber, etCvv, etNameOnCard;
    private Spinner spnYear, spnMonth;
    private CheckBox chkProcessFee;





    public PaymentFragmentSubCreditCard() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_sub_credit_card, container, false);

        etCardNumber = (EditText) view.findViewById(R.id.et_card_number);
        etCvv = (EditText) view.findViewById(R.id.et_cvv);
        etNameOnCard = (EditText) view.findViewById(R.id.et_name_on_card);

        spnMonth = (Spinner) view.findViewById(R.id.spn_month);
        spnYear = (Spinner) view.findViewById(R.id.spn_year);

        chkProcessFee = (CheckBox) view.findViewById(R.id.chk_process_fee);

        return view;

    }





}
