package c.com.app.android.insurance.adapt.registration.processscreens;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import c.com.app.android.insurance.adapt.R;
import c.com.app.android.insurance.adapt.registration.processscreens.AccountTypeListFragment.OnListFragmentInteractionListener;
import c.com.app.android.insurance.adapt.registration.processscreens.contents.AccountTypeContent;

import java.util.List;

/**
 * Recycler view class that allows yout to display list of account types.
 */
public class AccountTypeListRecyclerViewAdapter extends RecyclerView.Adapter<AccountTypeListRecyclerViewAdapter.ViewHolder> {





    private final List<AccountTypeContent.AccountTypeItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public AccountTypeListRecyclerViewAdapter(List<AccountTypeContent.AccountTypeItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_account_type_list_item, parent, false);

        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(position);
        holder.mTvAccountType.setText(mValues.get(position).name); // --> Display account type name.
        holder.mImgType.setImageResource(mValues.get(position).imageDir); // --> Setting up image...

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                }

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final ImageView mImgType;
        public final TextView mTvAccountType;

        public AccountTypeContent.AccountTypeItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mImgType = (ImageView) view.findViewById(R.id.img_account_type);
            mTvAccountType = (TextView) view.findViewById(R.id.tv_account_type);

        }

        @Override
        public String toString() {
            return super.toString() + "NULL";
        }

    }





}
