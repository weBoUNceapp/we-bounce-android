package c.com.app.android.insurance.adapt.registration.processscreens;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import c.com.app.android.insurance.adapt.R;
import c.com.app.android.insurance.adapt.registration.processscreens.SelectedResiliencyItemFragment.OnListFragmentInteractionListener;
import c.com.app.android.insurance.adapt.registration.processscreens.contents.SelectedResiliencyContent.SelectedResiliencyItem;
import c.com.app.android.insurance.adapt.registration.processscreens.dialogs.ContractDialogFragmentBase;

import java.util.List;

public class SelectedResiliencyItemRecyclerViewAdapter extends RecyclerView.Adapter<SelectedResiliencyItemRecyclerViewAdapter.ViewHolder> {





    private final List<SelectedResiliencyItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public SelectedResiliencyItemRecyclerViewAdapter(List<SelectedResiliencyItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_selected_resiliency_item, parent, false);

        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final int pos = position; // --> Needed to declare final to switch content selected for the CONTRACT DIALOG.

        holder.mItem = mValues.get(position);
        holder.mTvPriceAndContract.setText("PHP" + mValues.get(position).price + " covers " + mValues.get(position).duration + " month(s)");
        holder.mImgSelectedProduct.setImageResource(mValues.get(position).imageDir);

        // Setting up button click listener programatically for each product to display dialog right here.
        holder.mBtnPreviewContract.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Dialog returns index out of bounds
                FragmentManager fm = ((FragmentActivity)v.getRootView().getContext()).getSupportFragmentManager();
                DialogFragment dialog = new ContractDialogFragmentBase(pos);
                dialog.show(fm, "SEE CONTRACRT DIALOG");

            }

        });

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                }

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mTvPriceAndContract;
        public final Button mBtnPreviewContract;
        public final ImageView mImgSelectedProduct;
        public SelectedResiliencyItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mTvPriceAndContract = (TextView) view.findViewById(R.id.tv_product_price_and_contract);
            mBtnPreviewContract = (Button) view.findViewById(R.id.btn_preview_contract);
            mImgSelectedProduct = (ImageView) view.findViewById(R.id.img_selected_resiliency);

        }

        @Override
        public String toString() {
            return super.toString() + "NULL";
        }

    }





}
