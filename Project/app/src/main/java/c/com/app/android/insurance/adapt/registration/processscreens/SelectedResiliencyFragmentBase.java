package c.com.app.android.insurance.adapt.registration.processscreens;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import c.com.app.android.insurance.adapt.R;

/**
 * A fragment screen that previews user's selected resiliency products.
 */
public class SelectedResiliencyFragmentBase extends Fragment {





    private TextView tvTotalPrice;
    private float totalPrice = 999f;





    public SelectedResiliencyFragmentBase() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_selected_resiliency_base, container, false);

        tvTotalPrice = (TextView) view.findViewById(R.id.tv_total_price);

        /*

            TODO:
            Need to find a way to compute for the total price based on the
            number of products the user selected.

            Where variable:
            totalPrice = p1 + p2 + p3...

         */
        tvTotalPrice.setText("TOTAL: PHP" + totalPrice); // --> Total price must set on the TextView.

        return view;

    }





}
