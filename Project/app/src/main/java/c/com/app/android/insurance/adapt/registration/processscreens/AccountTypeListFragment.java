package c.com.app.android.insurance.adapt.registration.processscreens;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.android.insurance.adapt.R;
import c.com.app.android.insurance.adapt.registration.processscreens.contents.AccountTypeContent;

public class AccountTypeListFragment extends Fragment {





    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;





    public AccountTypeListFragment() {

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_account_type_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {

            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            if (mColumnCount <= 1) {

                recyclerView.setLayoutManager(new LinearLayoutManager(context));

            } else {

                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));

            }

            recyclerView.setAdapter(new AccountTypeListRecyclerViewAdapter(AccountTypeContent.ITEMS, mListener));

        }

        return view;

    }

    // These lines are commented due to error. Can live without these.
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }





    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(AccountTypeContent.AccountTypeItem item);

    }





}
