package c.com.app.android.insurance.adapt.registration.processscreens.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import c.com.app.android.insurance.adapt.R;

/**
 * Allows you to preview MI product's details.
 */
public class ContractDialogFragmentBase extends DialogFragment {





    private ImageView imgProduct;
    private TextView tvContractName;

    private int position = 0;
    private int[] imgDir;

    private String[] contractName;





    /** Where position index aligned relatively with selected MI product. */
    public ContractDialogFragmentBase(int position) {

        this.position = position;

        // Fetching image directory...
        imgDir = new int[6];
        imgDir[0] = R.drawable.resilience_options_calamity;
        imgDir[1] = R.drawable.resilience_options_health;
        imgDir[2] = R.drawable.resilience_options_livelihood;
        imgDir[3] = R.drawable.resilience_options_education;
        imgDir[4] = R.drawable.resilience_options_accidents;
        imgDir[5] = R.drawable.resilience_options_life;

        // TODO: Need unique contract/MI product name different from the resiliency option screen. (See slideshow on RESILIENCY CONTRACT DIALOG)
        contractName = new String[6];
        contractName[0] = "\nProtection from calamity";
        contractName[1] = "\nProtection for health";
        contractName[2] = "\nLivelihood Protection";
        contractName[3] = "\nEducation";
        contractName[4] = "\nProtection from accidents";
        contractName[5] = "\nLife insurance";

    }





    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_fragment_contract_dialog_base, null);

        builder.setView(view);

        imgProduct = (ImageView) view.findViewById(R.id.img_contract_product);
        tvContractName = (TextView) view.findViewById(R.id.tv_contract_name);

        // Implementing image selected + name...
        imgProduct.setImageResource(imgDir[position]);
        tvContractName.setText("SIMPLE CONTRACT" + contractName[position]);

        return builder.create();

    }





}
