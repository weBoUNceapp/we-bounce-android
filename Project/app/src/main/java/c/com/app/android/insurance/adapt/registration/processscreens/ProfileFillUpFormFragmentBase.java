package c.com.app.android.insurance.adapt.registration.processscreens;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.android.insurance.adapt.R;

/**
 * Fragment class that holds the base layer of the Profile Form.
 */
public class ProfileFillUpFormFragmentBase extends Fragment {

    public ProfileFillUpFormFragmentBase() {

        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_profile_fill_up_form_base, container, false);

    }
}
