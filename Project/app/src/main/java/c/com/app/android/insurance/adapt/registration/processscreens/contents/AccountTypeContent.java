package c.com.app.android.insurance.adapt.registration.processscreens.contents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import c.com.app.android.insurance.adapt.R;

/**
 * Each item of the list contains details of this account type.
 */
public class AccountTypeContent {





    public static final List<AccountTypeItem> ITEMS = new ArrayList<AccountTypeItem>();
    public static final Map<String, AccountTypeItem> ITEM_MAP = new HashMap<String, AccountTypeItem>();

    private static final int COUNT = 3;





    static {

        // Add some sample items.

        for (int i = 1; i <= COUNT; i++) {

            addItem(createItem(i-1));

        }

    }





    private static void addItem(AccountTypeItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.name, item);

    }





    private static AccountTypeItem createItem(int position) {

        // Setting up contract type name...
        String[] contractName = new String[3];
        contractName[0] = "Individual";
        contractName[1] = "Group/Association/Cooperative";
        contractName[2] = "Service/Product Provider";

        // Setting up image directory...
        int[] imgDir = new int[3];
        imgDir[0] = R.drawable.contract_type_individual;
        imgDir[1] = R.drawable.contract_type_associates;
        imgDir[2] = R.drawable.contract_type_service_provider;

        return new AccountTypeItem(contractName[position], "Item " + position, imgDir[position]);

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for (int i = 0; i < position; i++) {

            builder.append("\nMore details information here.");

        }

        return builder.toString();

    }





    /**
     * An "account type" item representing a piece of content.
     */
    public static class AccountTypeItem {

        public final String name;
        public final String content;
        public final int imageDir;

        public AccountTypeItem(String name, String content, int imageDir) {

            this.name = name;
            this.content = content;
            this.imageDir = imageDir;

        }

        @Override
        public String toString() {
            return content;
        }

    }





}
