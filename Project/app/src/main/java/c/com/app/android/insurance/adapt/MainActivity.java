package c.com.app.android.insurance.adapt;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {





    // BUTTONS
    private Button btnLog;
    private Button btnTest; // --> For debugging test. Two click listener events in one JAVA file.

    // FRAGMENT
    private FragmentManager fm;
    private FragmentTransaction ft;





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        btnLog = (Button) findViewById(R.id.btn_login_or_sign_up);
        btnTest = (Button) findViewById(R.id.btn_test);

    }





    public void onClick(View v) {

        // Run/show the LOGIN/SIGN-UP dialog panel.
        if(v == btnLog) {

//            btnLog.setVisibility(View.INVISIBLE);

            FragmentManager fm = ((FragmentActivity)v.getRootView().getContext()).getSupportFragmentManager();
            DialogFragment dialog = new LoginDialogFragment();
            dialog.show(fm, "LOGIN/SIGN-UP DIALOG");

        }

    }





    public void onClick2(View v) {

        if(v == btnTest) {

            System.exit(0);

        }

    }





}
