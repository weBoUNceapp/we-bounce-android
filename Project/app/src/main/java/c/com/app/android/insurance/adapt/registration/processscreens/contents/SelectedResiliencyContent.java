package c.com.app.android.insurance.adapt.registration.processscreens.contents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import c.com.app.android.insurance.adapt.R;

/**
 * Each item of the list contains details of the resiliency option selected.
 */
public class SelectedResiliencyContent {





    public static final List<SelectedResiliencyItem> ITEMS = new ArrayList<SelectedResiliencyItem>();
    public static final Map<String, SelectedResiliencyItem> ITEM_MAP = new HashMap<String, SelectedResiliencyItem>();

    private static final int COUNT = 6;





    static {

        // Add some sample items.

        for (int i = 1; i <= COUNT; i++) {

            addItem(createItem(i-1));

        }

    }





    private static void addItem(SelectedResiliencyItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.price, item);

    }





    private static SelectedResiliencyItem createItem(int position) {

        /*
        *
        *   TODO:
        *    Need price for each product and the duration of the selected product as well.
        *
        * */

        // Setting up image directory...
        int[] imgDir = new int[6];
        imgDir[0] = R.drawable.resilience_options_calamity;
        imgDir[1] = R.drawable.resilience_options_health;
        imgDir[2] = R.drawable.resilience_options_livelihood;
        imgDir[3] = R.drawable.resilience_options_education;
        imgDir[4] = R.drawable.resilience_options_accidents;
        imgDir[5] = R.drawable.resilience_options_life;

        return new SelectedResiliencyItem(String.valueOf(position), "Item " + position, imgDir[position], 999);

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for (int i = 0; i < position; i++) {

            builder.append("\nMore details information here.");

        }

        return builder.toString();

    }





    /**
     * A "selected resiliency option" item representing a piece of content.
     */
    public static class SelectedResiliencyItem {

        public final String price; // --> You can modify to float value if you wish.
        public final String content;

        public final int imageDir;
        public final int duration; // --> Contract for n month(s)

        public SelectedResiliencyItem(String price, String content, int imageDir, int duration) {

            this.price = price;
            this.content = content;
            this.imageDir = imageDir;
            this.duration = duration;

        }

        @Override
        public String toString() {
            return content;
        }

    }





}
