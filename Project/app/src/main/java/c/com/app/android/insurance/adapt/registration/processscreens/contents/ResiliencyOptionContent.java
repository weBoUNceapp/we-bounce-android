package c.com.app.android.insurance.adapt.registration.processscreens.contents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import c.com.app.android.insurance.adapt.R;

/**
 * Each item of the list contains details of the resiliency option selected.
 */
public class ResiliencyOptionContent {





    public static final List<ResiliencyItem> ITEMS = new ArrayList<ResiliencyItem>();
    public static final Map<String, ResiliencyItem> ITEM_MAP = new HashMap<String, ResiliencyItem>();

    private static final int COUNT = 6;





    static {

        // Add some sample items.

        for (int i = 1; i <= COUNT; i++) {

            addItem(createItem(i-1));

        }

    }





    private static void addItem(ResiliencyItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.productName, item);

    }





    private static ResiliencyItem createItem(int position) {

        // Setting up contract type name...
        String[] productName = new String[6];
        productName[0] = "Calamity";
        productName[1] = "Health";
        productName[2] = "Livelihood Asset";
        productName[3] = "Education";
        productName[4] = "Accident";
        productName[5] = "Life";

        // Setting up image directory...
        int[] imgDir = new int[6];
        imgDir[0] = R.drawable.resilience_options_calamity;
        imgDir[1] = R.drawable.resilience_options_health;
        imgDir[2] = R.drawable.resilience_options_livelihood;
        imgDir[3] = R.drawable.resilience_options_education;
        imgDir[4] = R.drawable.resilience_options_accidents;
        imgDir[5] = R.drawable.resilience_options_life;

        return new ResiliencyItem(productName[position], "Item " + position, imgDir[position]);

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for (int i = 0; i < position; i++) {

            builder.append("\nMore details information here.");

        }

        return builder.toString();

    }





    /**
     * An "resiliency option" item representing a piece of content.
     */
    public static class ResiliencyItem {

        public final String productName;
        public final String content;
        public final int imageDir;

        public ResiliencyItem(String productName, String content, int imageDir) {

            this.productName = productName;
            this.content = content;
            this.imageDir = imageDir;

        }

        @Override
        public String toString() {
            return content;
        }

    }





}
