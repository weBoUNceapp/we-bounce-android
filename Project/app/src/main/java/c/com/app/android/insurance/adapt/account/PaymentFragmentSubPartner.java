package c.com.app.android.insurance.adapt.account;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import c.com.app.android.insurance.adapt.R;


/**
 * Payment screen submenu for giving the users to choose their favorite payment partners.
 */
public class PaymentFragmentSubPartner extends Fragment {





    /*

        TODO: Find a way to detect user's selected payment partners.

        Conditions needed to achieve:
        - Need to know which of the following partners can support this app.

    */





    // TODO: Function of these radio buttons needed for selecting a partner. You may add another radio button as you wish.
    private RadioGroup radioPaymentPartners;
    private RadioButton[] radioBtnPartners;





    public PaymentFragmentSubPartner() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_sub_partner, container, false);

        radioPaymentPartners = (RadioGroup) view.findViewById(R.id.radio_grp_partners);

        radioBtnPartners = new RadioButton[3]; // --> You may add/increase array index as you wish.
        radioBtnPartners[0] = (RadioButton) view.findViewById(R.id.radio_btn_partner_1);
        radioBtnPartners[1] = (RadioButton) view.findViewById(R.id.radio_btn_partner_2);
        radioBtnPartners[2] = (RadioButton) view.findViewById(R.id.radio_btn_partner_3);

        return view;

    }





}
