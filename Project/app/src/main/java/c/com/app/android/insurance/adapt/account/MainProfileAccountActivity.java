package c.com.app.android.insurance.adapt.account;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import c.com.app.android.insurance.adapt.R;

public class MainProfileAccountActivity extends AppCompatActivity {





    /*

        TODO: Need functions for clickable images as buttons using ImageView (to adapt resizable image), not ImageButton.

         Conditions needed to achieve (may add additional layouts/fragments for view.):
        - Profile button function for payment management.
        - Notification button preview.
        - Message button and its layout, real time.
        - Invite button.

     */





    private ImageView imgProfile, imgNotification, imgMsg, imgInvite; // --> Used as clickable images.





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_profile_account);

        imgProfile = (ImageView) findViewById(R.id.img_profile);
        imgNotification = (ImageView) findViewById(R.id.img_notification);
        imgMsg = (ImageView) findViewById(R.id.img_msg);
        imgInvite = (ImageView) findViewById(R.id.img_invite);

        // Example as a clickable image view...
        imgProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Testing...
                System.exit(0);

            }

        });

    }




    
}
