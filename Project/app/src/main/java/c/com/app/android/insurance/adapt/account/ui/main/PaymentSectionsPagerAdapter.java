package c.com.app.android.insurance.adapt.account.ui.main;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import c.com.app.android.insurance.adapt.R;
import c.com.app.android.insurance.adapt.account.PaymentFragmentSubCreditCard;
import c.com.app.android.insurance.adapt.account.PaymentFragmentSubPartner;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class PaymentSectionsPagerAdapter extends FragmentPagerAdapter {





    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2};
    private Context mContext;





    public PaymentSectionsPagerAdapter(Context context, FragmentManager fm) {

        super(fm);
        mContext = context;

    }





    public PaymentSectionsPagerAdapter(FragmentManager fm) {

        super(fm);

    }





    @Override
    public Fragment getItem(int position) {

//        return PlaceholderFragment.newInstance(position + 1);

        Fragment fragment = null;

        switch(position) {

            case 0:
                fragment = new PaymentFragmentSubCreditCard();
                break;

            case 1:
                fragment = new PaymentFragmentSubPartner();
                break;

        }

        return fragment;

    }





    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

//        return mContext.getResources().getString(TAB_TITLES[position]);

        switch(position) {

            case 0:
                return "Credit/Debit Card";

            case 1:
                return "Payment Partner";

        }

        return null;

    }





    @Override
    public int getCount() {

        // Show 2 total pages.
        return 2;

    }





}