package c.com.app.android.insurance.adapt.registration.processscreens;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import c.com.app.android.insurance.adapt.R;
import c.com.app.android.insurance.adapt.registration.processscreens.ResiliencyOptionItemFragment.OnListFragmentInteractionListener;
import c.com.app.android.insurance.adapt.registration.processscreens.contents.ResiliencyOptionContent.ResiliencyItem;

import java.util.List;

/**
 * Recycler view class that allows yout to display list of resiliency options.
 */
public class ResiliencyOptionItemRecyclerViewAdapter extends RecyclerView.Adapter<ResiliencyOptionItemRecyclerViewAdapter.ViewHolder> {





    private final List<ResiliencyItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public ResiliencyOptionItemRecyclerViewAdapter(List<ResiliencyItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_resiliency_option_item, parent, false);

        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(position);
        holder.mTvResiliencyType.setText(mValues.get(position).productName);
        holder.mImgResiliencyType.setImageResource(mValues.get(position).imageDir);

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                }

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final ImageView mImgResiliencyType;
        public final TextView mTvResiliencyType;
        public ResiliencyItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mImgResiliencyType = (ImageView) view.findViewById(R.id.img_resiliency_type);
            mTvResiliencyType = (TextView) view.findViewById(R.id.tv_resiliency_type);

        }

        @Override
        public String toString() {
            return super.toString() + "NULL";
        }

    }

}
