package c.com.app.android.insurance.adapt;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import c.com.app.android.insurance.adapt.account.MainProfileAccountActivity;
import c.com.app.android.insurance.adapt.registration.RegistrationActivity;


/**
 * Fragment activity for login/sign-up.
 */
public class LoginDialogFragment extends DialogFragment {





    private ImageButton btnFacebook;
    private ImageButton btnGoogle;
    private ImageButton btnMobile;





    public LoginDialogFragment() {

        // Required empty public constructor

    }





    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_fragment_login_dialog, null);

        builder.setView(view);

        // Login w/ Facebook Account Button
        btnFacebook = (ImageButton) view.findViewById(R.id.img_btn_facebook);
        btnFacebook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Need a way to connect/register account using Facebook.

            }

        });

        // Login w/ Google Account Button
        btnGoogle = (ImageButton) view.findViewById(R.id.img_btn_google);
        btnGoogle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Need a way to connect/register account using Google.
                // TODO: In the meantime, I temporarily added it for testing view. (Main Account)
//                startActivity(new Intent(getActivity().getApplicationContext(), PaymentFragment.class)); // --> Temporary disabled due to class cast error with tab viewer in fragment activity.
                startActivity(new Intent(getActivity().getApplicationContext(), MainProfileAccountActivity.class));

            }

        });

        // Login w/ Mobile Button
        btnMobile = (ImageButton) view.findViewById(R.id.img_btn_phone);
        btnMobile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Need a way to connect/register account using mobile phone number.
                // TODO: In the meantime, I temporarily added it for testing view. (Registration Activity)
                // TODO: Registration activity must be appeared after signed-up either any of the three (FB, Google or mobile phone no.)
                startActivity(new Intent(getActivity().getApplicationContext(), RegistrationActivity.class));

            }

        });

        return builder.create();

    }





}
