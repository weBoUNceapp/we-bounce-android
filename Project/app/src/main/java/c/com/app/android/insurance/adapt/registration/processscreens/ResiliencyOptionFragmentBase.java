package c.com.app.android.insurance.adapt.registration.processscreens;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.android.insurance.adapt.R;

/**
 * Fragment screen contains the base layout for resiliency product list.
 */
public class ResiliencyOptionFragmentBase extends Fragment {

    public ResiliencyOptionFragmentBase() {

        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_resiliency_option_base, container, false);

    }

}
