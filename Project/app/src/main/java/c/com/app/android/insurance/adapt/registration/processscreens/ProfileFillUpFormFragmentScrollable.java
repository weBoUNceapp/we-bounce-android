package c.com.app.android.insurance.adapt.registration.processscreens;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.android.insurance.adapt.R;

/**
 * Fragment layout contains list of details to fill-out your information.
 */
public class ProfileFillUpFormFragmentScrollable extends Fragment {





    private static boolean isAllFieldsFinished; // --> Needed/checks if user has filled all the fields in profile form.





    public ProfileFillUpFormFragmentScrollable() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile_fill_up_form_scrollable, container, false);

        return view;

    }





}
