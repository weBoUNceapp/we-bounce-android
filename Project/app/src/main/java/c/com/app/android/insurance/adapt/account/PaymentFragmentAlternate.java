package c.com.app.android.insurance.adapt.account;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import c.com.app.android.insurance.adapt.R;

/**
 * Alternate fragment view of the payment options screen w/o the use of tab view.
 */
public class PaymentFragmentAlternate extends Fragment {





    // BUTTONS
    private Button btnCredit, btnPartner;

    // FRAGMENT
    private FragmentManager fm;
    private FragmentTransaction ft;





    public PaymentFragmentAlternate() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_alternate, container, false);

        fm = getFragmentManager();

        btnCredit = (Button) view.findViewById(R.id.btn_credit_card);
        btnPartner = (Button) view.findViewById(R.id.btn_payment_partners);

        final Fragment creditCardFragment = new PaymentFragmentSubCreditCard();
        final Fragment partnerFragment = new PaymentFragmentSubPartner();

        btnCredit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ft = fm.beginTransaction();
                ft.replace(R.id.payment_tab_fragment, creditCardFragment);
                ft.commit();

            }

        });

        btnPartner.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ft = fm.beginTransaction();
                ft.replace(R.id.payment_tab_fragment, partnerFragment);
                ft.commit();

            }

        });

        return view;

    }





}
