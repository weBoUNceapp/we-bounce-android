package c.com.app.android.insurance.adapt.registration;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import c.com.app.android.insurance.adapt.R;
import c.com.app.android.insurance.adapt.registration.processscreens.AccountTypeFragment;
import c.com.app.android.insurance.adapt.registration.processscreens.ProfileFillUpFormFragmentBase;
import c.com.app.android.insurance.adapt.registration.processscreens.ResiliencyOptionFragmentBase;
import c.com.app.android.insurance.adapt.registration.processscreens.SelectedResiliencyFragmentBase;

public class RegistrationActivity extends AppCompatActivity {





    // BUTTONS
    private Button btnPrev;
    private Button btnNext;

    // FRAGMENT
    private Fragment[] formFragments;
    private FragmentManager fm;
    private FragmentTransaction ft;
    private int fragIndex = 0; // --> To be used in switching subforms of registration.





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        fm = getSupportFragmentManager();
        formFragments = new Fragment[4];
        formFragments[0] = new AccountTypeFragment();
        formFragments[1] = new ProfileFillUpFormFragmentBase();
        formFragments[2] = new ResiliencyOptionFragmentBase();
        formFragments[3] = new SelectedResiliencyFragmentBase();

        btnPrev = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);

        // Hide Prev Button from the start.
        if(fragIndex == 0) {

            btnPrev.setVisibility(View.INVISIBLE);

        }

    }





    public void onRegistrationClick(View v) {

        // When user clicked a button from these two...
        if(v == btnPrev) {

            fragIndex--;

            if(fragIndex < 0) fragIndex = 0;

        } else if(v == btnNext) {

            fragIndex++;

            if(fragIndex > 3) fragIndex = 3;

        }

        // Hide prev button if reached at the end.
        if(fragIndex == 0) {

            btnPrev.setVisibility(View.INVISIBLE);

        } else {

            btnPrev.setVisibility(View.VISIBLE);

        }

        // Change text if reached the last subform.
        if(fragIndex == 3) {

            btnNext.setText("DONE");

        } else {

            btnNext.setText("NEXT");

        }

        // Picking the selected subform of regsitration based on the user's click.
        switch(fragIndex) {

            case 0:
                ft = fm.beginTransaction();
                ft.replace(R.id.registration_fragment_main, formFragments[0]);
                ft.commit();
                break;

            case 1:
                ft = fm.beginTransaction();
                ft.replace(R.id.registration_fragment_main, formFragments[1]);
                ft.commit();
                break;

            case 2:
                ft = fm.beginTransaction();
                ft.replace(R.id.registration_fragment_main, formFragments[2]);
                ft.commit();
                break;

            case 3:
                ft = fm.beginTransaction();
                ft.replace(R.id.registration_fragment_main, formFragments[3]);
                ft.commit();

        }

    }





}
